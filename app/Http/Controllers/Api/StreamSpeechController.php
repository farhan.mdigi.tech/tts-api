<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class StreamSpeechController extends Controller
{
    public function __invoke(string $uuid)
    {
        try {
            return Storage::download('tts/' . $uuid . '.wav');
        } catch (\Exception $e) {
            Log::error('No speech found.', [
                'error' => $e->getMessage()
            ]);
            return response()->json([
                'error' => 'Speech not found.'
            ], 404);
        }
    }
}
