<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TextToSpeechRequest;
use App\Services\SpeechService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TextToSpeechController extends Controller
{

    public function __construct(private SpeechService $service)
    {
    }

    public function __invoke(TextToSpeechRequest $request)
    {
        $params = $request->validated();
        try {
            $uuid = $this->service->textToAudio($params['text']);
            return response()->json([
                'audio' => route('stream-speech', $uuid),
            ]);
        } catch (\Exception $e) {
            Log::error('Unable to generate tts', [
                'error' => $e->getMessage(),
            ]);
            return response()->json([
                'audio' => null,
                'error' => 'Sorry, we are unable to serve your request at the moment.'
            ], 500);
        }
    }
}
