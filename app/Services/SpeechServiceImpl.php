<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class SpeechServiceImpl implements SpeechService
{

    protected string $espeak;

    public function __construct()
    {
        $this->espeak = config('speak.binary');
    }

    /**
     * @inheritDoc
     */
    public function textToAudio(string $text): string
    {
        $uuid = Str::uuid()->toString();
        Storage::makeDirectory('tts');
        $path = Storage::path('tts') . DIRECTORY_SEPARATOR . $uuid . '.wav';
        Process::run($this->espeak.' -w ' . $path . ' -vid "' . $text . '"');
        return $uuid;
    }
}
