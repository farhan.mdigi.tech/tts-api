<?php

namespace App\Services;

use Exception;

interface SpeechService
{
    /**
     * @param string $text
     * @return string
     * @throws Exception
     */
    public function textToAudio(string $text): string;
}
