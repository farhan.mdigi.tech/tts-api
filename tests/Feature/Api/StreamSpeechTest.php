<?php

namespace Tests\Feature\Api;

use App\Services\SpeechService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StreamSpeechTest extends TestCase
{
    /** @test */
    public function it_can_stream_audio()
    {
        $service = $this->app->make(SpeechService::class);
        $uuid = $service->textToAudio('Hanya test aja');
        $this->get(route('stream-speech', $uuid))->assertStatus(200)
        ->assertHeader('Content-Type', 'audio/x-wav');
    }
}
