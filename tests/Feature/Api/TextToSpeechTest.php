<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TextToSpeechTest extends TestCase
{
    /** @test */
    public function it_can_generate_audio()
    {
        $this->post(route('text-to-speech'), [
            'text' => 'Aku adalah audio yang dibuat lewat tes',
        ])->assertStatus(200)
        ->assertSee('audio');
    }
}
