## TTS (Text to Speech) API

### Features:
1.  Convert text into audio speech
2. Stream speech audio file that are generated from feature __1__

### Notes:
- This project is using speech synthesizer [eSpeak](https://github.com/espeak-ng/espeak-ng) so make sure you already install it on the machine which this API going to run.
