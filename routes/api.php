<?php

use App\Http\Controllers\Api\StreamSpeechController;
use App\Http\Controllers\Api\TextToSpeechController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/text-to-speech', TextToSpeechController::class)->name('text-to-speech');
Route::get('/stream-speech/{uuid}', StreamSpeechController::class)->name('stream-speech');
