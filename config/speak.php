<?php

return [
    /***
     * You can customize `espeak` binary according which version is installed on your machine.
     * The default is using `espeak`, but you can change it into `espeak-ng`
     */
    'binary' => env('ESPEAK_BINARY', 'espeak'),
];
